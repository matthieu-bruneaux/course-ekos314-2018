### * Setup

TOP_DIR=$(shell git rev-parse --show-toplevel)

### ** Colors

# https://stackoverflow.com/questions/5947742/how-to-change-the-output-color-of-echo-in-linux
# https://siongui.github.io/2016/02/18/makefile-echo-color-output/#id3
RED = "\\033[31m"
GREEN = "\\033[92m"
NC = "\\033[0m"

### * Rules

### ** help

# http://swcarpentry.github.io/make-novice/08-self-doc/
# https://stackoverflow.com/questions/19129485/in-linux-is-there-a-way-to-align-text-on-a-delimiter
.PHONY: help
help: Makefile
	@printf "\n"
	@printf "Please use 'make <target>' where <target> is one of\n"
	@printf "\n"
	@sed -n 's/^## /    /p' $< | column -t -s ":"

### ** buildSite

## buildSite : build the site into docs/ folder
.PHONY: buildSite
buildSite: clean
	@printf "\n"
	@printf "$(GREEN)*** Building the site ***$(NC)\n"
	@printf "\n"
	@mkdir -p docs/
	@cp -r practicals docs/
	@Rscript -e "rmarkdown::render('index.Rmd')"
	@mv index.html docs/
	@cp customization.css docs/

### ** clean

## clean : remove automatically generated files
.PHONY: clean
clean:
	@rm -fr docs/*
