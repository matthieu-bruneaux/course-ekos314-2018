## Course material for EKOS 314 (2018)

This course material was created by Sara Calhi and Andrés López-Sepulcre for
the course **Foundations of Statistics for Ecology and Evolution** (EKOS314)
given at the University of Jyväskylä.
